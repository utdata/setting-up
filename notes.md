# Notes

## Resources

* [Install and configure Git](https://confluence.atlassian.com/get-started-with-bitbucket/install-and-set-up-git-860009658.html)
* [Learn Git and Bitbucket](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud?ep_click_type=primary)
* [Get started with Bitbucket](https://confluence.atlassian.com/get-started-with-bitbucket/get-started-with-bitbucket-cloud-856845168.html)
* [Academic license](https://www.atlassian.com/software/views/bitbucket-academic-license)
* [SSH keys](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html)

## VCS on Windows

* I need to explore how the Terminal in VS Code works. Does it understand unix commands?
* I'd like to find something equivalent to git-bash-prompt, but those features may already be baked into Git Bash.
* We might consider [Chocolatey](https://chocolatey.org/) package manager.

## Scaffold managers

### generator-politico-interactives

* I've installed node and yarn, but my first attempt complained that I didn't have gulp.
