# Setting up your computer for development

I really recommend that you use your own computer in my coding and data clases, especially if you own a Macintosh. Windows users will have more of a challenge, especially since this is the first year of a revamped coding class, but we'll work through issues.

If you are using a lab computer, most (but perhaps not all) of this will be done already.

Pay attention to verbal details given, as not all things may be installed or at the same time.

* [Macintosh](macintosh.md)
* [Windows](windows.md)
* [Linux](https://giphy.com/gifs/lol-laughing-muttley-3oEjHAUOqG3lSS0f1C)